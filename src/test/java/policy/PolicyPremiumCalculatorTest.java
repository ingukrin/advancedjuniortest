package policy;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import policy.object.PolicyObject;
import policy.object.PolicyObjectType;
import policy.object.subObjects.PolicySubObject;
import policy.object.subObjects.PolicySubObjectType;
import policy.object.subObjects.RiskType;

@RunWith(MockitoJUnitRunner.class)
public class PolicyPremiumCalculatorTest {

	@Mock
	private Policy policy;
	@Mock
	private PolicyObject policyObject;

	@InjectMocks
	private PolicyPremiumCalculator premiumCalculator;

	@Test
	public void calculatesZeroPremiumWhenNoObjectsExist() {
		given(policy.getPolicyObjects()).willReturn(emptyList());
		BigDecimal premium = premiumCalculator.calculate(policy);
		assertThat(premium).isZero();
		System.out.println("Expected value -->0.00  CalculatedPremiun --> "+ premium);
	}

@Test
	public void calculatesPolicyPremiumV1(){
		policy=Policy.builder()
				.policyNumber("LV20-02-100000-6")
				.policyStatus(PolicyStatus.REGISTERED)
				.policyObjects(
						List.of( new PolicyObject( PolicyObjectType.REAL_ESTATE,
								List.of(new PolicySubObject(PolicySubObjectType.TV, BigDecimal.valueOf(492), RiskType.FIRE),
										new PolicySubObject(PolicySubObjectType.MUSIC_PLAYER, BigDecimal.valueOf(8),RiskType.FIRE),
										new PolicySubObject(PolicySubObjectType.MUSIC_PLAYER,BigDecimal.valueOf(102.51),RiskType.THEFT)
								))
						)
				).build();
		BigDecimal premium = premiumCalculator.calculate(policy);
		assertThat(premium).isEqualByComparingTo(BigDecimal.valueOf(17.13));
		System.out.println("Expected value -->17.13  CalculatedPremiun --> "+ premium);

}


	@Test
	public void calculatesPolicyPremiumV2(){
		policy=Policy.builder()
				.policyNumber("LV20-02-100000-6")
				.policyStatus(PolicyStatus.REGISTERED)
				.policyObjects(
						List.of( new PolicyObject( PolicyObjectType.REAL_ESTATE,
								List.of(new PolicySubObject(PolicySubObjectType.TV, BigDecimal.valueOf(100),RiskType.FIRE),
										new PolicySubObject(PolicySubObjectType.MUSIC_PLAYER,BigDecimal.valueOf(8),RiskType.THEFT)
								))
						)
				).build();
		BigDecimal premium = premiumCalculator.calculate(policy);
		assertThat(premium).isEqualByComparingTo(BigDecimal.valueOf(2.28));
		System.out.println("Expected value -->2.28  CalculatedPremiun --> "+ premium);

	}


	@Test
	public void calculatesPolicyPremiuFireBefore100() {
		given(policy.getPolicyObjects()).willReturn(singletonList(policyObject));
		given(policyObject.calculateObjectSumInsured(RiskType.FIRE)).willReturn(BigDecimal.valueOf(10));
		BigDecimal premium = premiumCalculator.calculate(policy);
		assertThat(premium).isEqualTo(BigDecimal.valueOf(0.14));
		System.out.println("Expected value -->0.14  CalculatedPremiun --> "+ premium);
	}

	@Test
	public void calculatesPolicyPremiuFireBiggerThan100() {
		given(policy.getPolicyObjects()).willReturn(singletonList(policyObject));
		given(policyObject.calculateObjectSumInsured(RiskType.FIRE)).willReturn(BigDecimal.valueOf(101));
		BigDecimal premium = premiumCalculator.calculate(policy);
		assertThat(premium).isEqualTo(BigDecimal.valueOf(2.42));
		System.out.println("Expected value -->2.42  CalculatedPremiun --> "+ premium);
	}

	@Test
	public void calculatesPolicyPremiuFireQueal100() {
		given(policy.getPolicyObjects()).willReturn(singletonList(policyObject));
		given(policyObject.calculateObjectSumInsured(RiskType.FIRE)).willReturn(BigDecimal.valueOf(100));
		BigDecimal premium = premiumCalculator.calculate(policy);
		assertThat(premium).isEqualTo(BigDecimal.valueOf(1.40).setScale(2));
		System.out.println("Expected value -->1.40 CalculatedPremiun --> "+ premium);
	}

	@Test
	public void calculatesPolicyPremiumTheftLessThan15() {
		given(policy.getPolicyObjects()).willReturn(singletonList(policyObject));
		given(policyObject.calculateObjectSumInsured(RiskType.THEFT)).willReturn(BigDecimal.valueOf(10));
		BigDecimal premium = premiumCalculator.calculate(policy);
		assertThat(premium).isEqualTo(BigDecimal.valueOf(1.10).setScale(2));
		System.out.println("Expected value -->1.10 CalculatedPremiun --> "+ premium);
	}

	@Test
	public void calculatesPolicyPremiumTheftLessEqual15() {
		given(policy.getPolicyObjects()).willReturn(singletonList(policyObject));
		given(policyObject.calculateObjectSumInsured(RiskType.THEFT)).willReturn(BigDecimal.valueOf(15));
		BigDecimal premium = premiumCalculator.calculate(policy);
		assertThat(premium).isEqualTo(BigDecimal.valueOf(0.75).setScale(2));
		System.out.println("Expected value --> 0.75 CalculatedPremiun --> "+ premium);
	}

	@Test
	public void calculatesPolicyPremiumTheftGreaterThan15() {
		given(policy.getPolicyObjects()).willReturn(singletonList(policyObject));
		given(policyObject.calculateObjectSumInsured(RiskType.THEFT)).willReturn(BigDecimal.valueOf(20));
		BigDecimal premium = premiumCalculator.calculate(policy);
		assertThat(premium).isEqualTo(BigDecimal.valueOf(1).setScale(2));
		System.out.println("Expected value -->1.00  CalculatedPremiun --> "+ premium);
	}






}