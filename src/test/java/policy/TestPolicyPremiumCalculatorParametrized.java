package policy;

import static org.junit.Assert.*;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import policy.object.PolicyObject;
import policy.object.subObjects.RiskType;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(Parameterized.class)
public class TestPolicyPremiumCalculatorParametrized {
    private BigDecimal testInput;
    private BigDecimal testOutput;
    private RiskType riskType;

    public TestPolicyPremiumCalculatorParametrized(BigDecimal testInput, BigDecimal testOutput, RiskType riksType) {
        this.testInput = testInput;
        this.testOutput = testOutput;
        this.riskType = riksType;
    }

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private Policy policy;

    @Mock
    private PolicyObject policyObject;

    @InjectMocks
    private PolicyPremiumCalculator premiumCalculator;

    @Parameterized.Parameters
    public static Collection<Object[]> getTestParameters(){
        return Arrays.asList(new Object[][]{

            {BigDecimal.valueOf(10), BigDecimal.valueOf(0.14), RiskType.FIRE},   ////RiskType.FIRE
            {BigDecimal.valueOf(101), BigDecimal.valueOf(2.42), RiskType.FIRE},  ////RiskType.FIRE
            {BigDecimal.valueOf(100), BigDecimal.valueOf(1.40), RiskType.FIRE}, ////RiskType.FIRE
            {BigDecimal.valueOf(10), BigDecimal.valueOf(1.10), RiskType.THEFT},    /////RiskType.THEFT
            {BigDecimal.valueOf(15), BigDecimal.valueOf(0.75), RiskType.THEFT},    /////RiskType.THEFT
            {BigDecimal.valueOf(20), BigDecimal.valueOf(1), RiskType.THEFT}    /////RiskType.THEFT
        });
    }
    @Test
    public void calculatesPolicyPremiumTheftGreaterThan15() {
        given(policy.getPolicyObjects()).willReturn(singletonList(policyObject));
        given(policyObject.calculateObjectSumInsured(riskType)).willReturn(testInput);
        BigDecimal premium = premiumCalculator.calculate(policy);
        assertThat(premium).isEqualTo(testOutput.setScale(2));
        System.out.println("Parametrized test: Expected value -->"+testOutput+"  CalculatedPremiun --> "+ premium);
    }
}
