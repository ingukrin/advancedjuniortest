package policy;

import policy.object.subObjects.RiskType;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class PolicyPremiumCalculator implements PremiumCalculator{

    public BigDecimal calculateSumInsured(Policy policy, RiskType riskType) {
        // Calculate Sum of policy by risk type
        return  policy.getPolicyObjects().stream().map(item->item
                .calculateObjectSumInsured(riskType))
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal calculateRiskType(Policy policy, RiskType riskType) {
        // Premium calculated by sumIsured*coificient. Coificent depends on sumIsured amount.
        BigDecimal coificent;
        BigDecimal  sumInsured;
        if(riskType.equals(RiskType.FIRE)){
            sumInsured = calculateSumInsured(policy, RiskType.FIRE);
            coificent= BigDecimal.valueOf((sumInsured.compareTo(BigDecimal.valueOf(100))==1)?0.024:0.014);
        }
        else
        {
            sumInsured = calculateSumInsured(policy, RiskType.THEFT);
            coificent= BigDecimal.valueOf((sumInsured.compareTo(BigDecimal.valueOf(15))>=0)?0.05:0.11);
        }
        return   sumInsured.multiply(coificent);
    }

    public BigDecimal calculate(Policy policy){
        return  (calculateRiskType(policy,RiskType.FIRE).add(calculateRiskType(policy,RiskType.THEFT))).setScale(2, RoundingMode.HALF_UP);
    }



}
