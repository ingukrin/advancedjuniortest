package policy.object.subObjects;

public enum RiskType {
    FIRE,
    THEFT
}
