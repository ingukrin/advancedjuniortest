package policy.object.subObjects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


@Getter
@AllArgsConstructor
public class PolicySubObject {
//    Policy sub-objects can be related only to one policy object and have 3 attributes:
//    Sub-object name --> e.g. TV
//    Sum insured --> Cost that will be covered by insurance
//    Risk type --> e.g. FIRE, THEFT
    private PolicySubObjectType subObjectName;
    private BigDecimal subObjectSumInsured;
    private RiskType riskType;
}
