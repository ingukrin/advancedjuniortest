package policy.object.subObjects;

public enum PolicySubObjectType {
    TV,
    VACCUM_CLEANER,
    PC,
    RADIO,
    MUSIC_PLAYER
}
