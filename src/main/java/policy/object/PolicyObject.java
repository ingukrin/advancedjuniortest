package policy.object;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

//import lombok.Builder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import policy.object.subObjects.PolicySubObject;
import policy.object.subObjects.RiskType;

@Getter
@AllArgsConstructor
public class PolicyObject {

//	Policy objects can have multiple sub-objects and can be related only to one policy
//	Policy objects have 2 attributes:
//	Object name --> e.g. A House
//	Sub-objects --> Collection of none or multiple sub-objects

	private PolicyObjectType policyObjectType;
	private List<PolicySubObject> policySubObjects;

	public BigDecimal calculateObjectSumInsured(RiskType riskType){
		//Calculate Sum Insured for each risk type
		return policySubObjects.stream()
				.filter(data->data.getRiskType().equals(riskType))
				.map(data ->data.getSubObjectSumInsured())
				.filter(Objects::nonNull)
				.reduce(BigDecimal.ZERO,BigDecimal::add);
	}
	

}
