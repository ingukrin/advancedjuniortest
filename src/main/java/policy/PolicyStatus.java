package policy;

public enum PolicyStatus {
    REGISTERED,
    APPROVED,
    CLOSED,
    CANCELED
}
