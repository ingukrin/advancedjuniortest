package policy;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import policy.object.PolicyObject;

@Builder
@Getter
public class Policy {

//Policy can have multiple policy objects and each policy object can have multiple sub-objects.
//Policy has 3 attributes:
// Policy number e.g. LV20-02-100000-5
// Policy status e.g. REGISTERED, APPROVED
//Policy objects Collection of one or multiple objects

	private String policyNumber;
	private PolicyStatus policyStatus;
	private final List<PolicyObject> policyObjects;



}